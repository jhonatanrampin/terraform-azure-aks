variable "environment" {
  description = "The environment name. Used to name and tag resources"
  type        = string
  default     = "dev"
}

variable "prefix" {
  description = "The prefix which should be used for all resources in this example"
  type        = string
  default     = "jrampin-labs"
}

variable "location" {
  description = "The Azure Region in which all resources in this example should be created."
  type        = string
  default     = "Australia East"
}